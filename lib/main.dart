import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_chart_task/line_charts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  List<String> months =
  ['January', 'February', 'March', 'April','May','June','July',
    'August','September','October','November','December'];
  DateTime date;
  String currentMonthName;
  int monthValue;
  TabController tabController;
  int currentIndex=0;
  @override
  void initState() {

  date=DateTime.now();
   monthValue=date.month;
   if(monthValue<12&&monthValue>0) {
     currentMonthName = months[monthValue - 1];
   }
   currentIndex=0;
  }
  @override
  Widget build(BuildContext context) {
    return
      DefaultTabController(
        length: 4,
          child: Builder(builder: (BuildContext context)
    {
      tabController=DefaultTabController.of(context);
      tabController.addListener(() {
        if (tabController.indexIsChanging) {
          setState(() {
            currentIndex=tabController.index;
          });
        }
      });
      tabController.animation.addListener(() {
          setState(() {
            currentIndex=(tabController.animation.value).round();
          });
      });
    return  Scaffold(
        backgroundColor: Colors.white.withOpacity(0.95),
        appBar:
        AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title:
          Container(
              alignment: Alignment.bottomLeft,
              child: Text(
                '${currentMonthName}',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 40,
                    fontWeight: FontWeight.bold
                ),

              )
          ),
          bottom: PreferredSize(
            preferredSize: Size(MediaQuery
                .of(context)
                .size
                .width, 50),
            child: Container(
              padding: EdgeInsets.only(bottom: 10),
              child: TabBar(
                controller: tabController,
                indicatorColor: Colors.white.withOpacity(0.1),
                tabs: [
                  currentIndex == 0 ? Container(
                    alignment: Alignment.center,
                    height: 30,
                    width: 100,
                    decoration: BoxDecoration(
                      color: Color(0xff02d39a).withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      'Day',
                      style: TextStyle(color: Colors.black,
                          fontSize: 12
                      ),
                    ),
                  ) :
                  Text(
                    'Day',
                    style: TextStyle(color: Colors.grey.shade500,
                        fontSize: 12
                    ),
                  ),
                  currentIndex == 1 ?   Container(
                    alignment: Alignment.center,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Color(0xff02d39a).withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Week',
                      style: TextStyle(color: Colors.black,
                          fontSize: 12
                      ),),
                  ):
                  Text('Week',
                    style: TextStyle(color: Colors.grey.shade500,
                        fontSize: 12
                    ),),
               currentIndex==2?   Container(
                    alignment: Alignment.center,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Color(0xff02d39a).withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Month',
                      style: TextStyle(color: Colors.black,
                          fontSize: 12
                      ),),
                  ):
               Text('Month',
                 style: TextStyle(color: Colors.grey.shade500,
                     fontSize: 12
                 ),),
                  currentIndex==3?Container(
                    alignment: Alignment.center,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Color(0xff02d39a).withOpacity(0.1),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(' 3 Months ',
                      style: TextStyle(color: Colors.black,
                          fontSize: 12
                      ),),
                  ):
                  Text(' 3 Months ',
                    style: TextStyle(color: Colors.grey.shade500,
                        fontSize: 12
                    ),),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
            controller: tabController,
            children: [Container(), Container(), LineCharts(), Container(),]
        ),
      );
    }
          )
    );
  }

}

