class MonthlyExpenses {
  String months;
  Expenses expenses;
  int total;

  MonthlyExpenses({this.months, this.expenses, this.total});

  MonthlyExpenses.fromJson(Map<String, dynamic> json) {
    months = json['Months'];
    expenses = json['Expenses'] != null
        ? new Expenses.fromJson(json['Expenses'])
        : null;
    total = json['Total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Months'] = this.months;
    if (this.expenses != null) {
      data['Expenses'] = this.expenses.toJson();
    }
    data['Total'] = this.total;
    return data;
  }
}

class Expenses {
  int shopping;
  int food;
  int travel;
  int fuel;
  int eMI;
  int others;

  Expenses(
      {this.shopping,
        this.food,
        this.travel,
        this.fuel,
        this.eMI,
        this.others});

  Expenses.fromJson(Map<String, dynamic> json) {
    shopping = json['Shopping'];
    food = json['Food'];
    travel = json['Travel'];
    fuel = json['Fuel'];
    eMI = json['EMI'];
    others = json['Others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Shopping'] = this.shopping;
    data['Food'] = this.food;
    data['Travel'] = this.travel;
    data['Fuel'] = this.fuel;
    data['EMI'] = this.eMI;
    data['Others'] = this.others;
    return data;
  }
}