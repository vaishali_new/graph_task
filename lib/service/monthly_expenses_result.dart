class MonthlyExpensesResult {
  String months;
  ExpensesResult expenses;
  int total;
}
class ExpensesResult {
  int shopping;
  int food;
  int travel;
  int fuel;
  int eMI;
  int others;
}