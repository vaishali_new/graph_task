import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:line_chart_task/service/monthly_expenses_result.dart';
import 'package:line_chart_task/util_constants.dart';
// import 'package:line_chart_task/util_constants.dart';
class LineCharts extends StatefulWidget {
  @override
  _LineChartsState createState() => _LineChartsState();

}

class _LineChartsState extends State<LineCharts> {
  List<Color> gradientColors = [
    const Color(0xff02d39a).withOpacity(0.7),
    const Color(0xff02d39a).withOpacity(0.6),
    const Color(0xff02d39a).withOpacity(0.3),
    const Color(0xff02d39a).withOpacity(0.2),
    const Color(0xff02d39a).withOpacity(0.1),
    const Color(0xff02d39a).withOpacity(0.0),
  ];
  int itemSelectedIndex=-1;
  Color itemSelectedColor;
  double itemSelectedValue;
  int totalValue,totalExpenditure, monthValue;
  List<FlSpot> spotsData1 ;
  List<FlSpot> spotsData2 ;
  MonthlyExpensesResult currentMonthResult;
  RegExp reg = new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  Function mathFunc = (Match match) => '${match[1]},';
  ScrollController scrollController;
  @override
  void initState() {
    scrollController=ScrollController();
    spotsData1 = UtilConstants().getSpotData1();
  spotsData2 = UtilConstants().getSpotData2();
    currentMonthResult = UtilConstants().getMonthResult();
   totalValue=currentMonthResult.total;
   totalExpenditure=UtilConstants().getTotalExpenditure(currentMonthResult)??0;
  }
  @override
  Widget build(BuildContext context) {
    const double cutOffYValue = 0.0;
    const yearTextStyle =
    TextStyle(fontSize: 10, color: Colors.black, fontWeight: FontWeight.bold);
    scrollController??=ScrollController();
    return
      ListView(
        controller: scrollController,
        children: [
          Container(
            padding: EdgeInsets.only(top: MediaQuery
                .of(context)
                .size
                .height * 0.05,
              bottom: MediaQuery
                  .of(context)
                  .size
                  .height * 0.05,

            ),
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height / 2,
            child: LineChart(
              LineChartData(
                  lineTouchData: LineTouchData(enabled: false),
                  lineBarsData: [
                    LineChartBarData(
                      spots: spotsData1 ?? [FlSpot(0, 4.6)],
                      isCurved: true,
                      barWidth: 2,
                      colors: [
                        Color(0xff02d39a)
                      ],
                      belowBarData: BarAreaData(
                        gradientFrom: Offset(0.0, 0.0),
                        gradientTo: Offset(0.0, 1),
                        gradientColorStops: [
                          0.1,
                          0.2,
                          0.4,
                          0.6,
                          0.8,
                          1
                        ],
                        show: true,
                        colors: gradientColors,
                        cutOffY: cutOffYValue,
                        applyCutOffY: false,
                      ),
                      aboveBarData: BarAreaData(
                        show: true,
                        colors: [Colors.red.withOpacity(0.6)],
                        cutOffY: cutOffYValue,
                        applyCutOffY: true,
                      ),
                      dotData: FlDotData(
                        show: false,
                      ),

                    ),
                    LineChartBarData(
                      spots: spotsData2 ?? [],
                      dashArray: [2, 4],
                      isCurved: true,
                      colors: [Colors.grey.withOpacity(0.5)],
                      barWidth: 2,
                      isStrokeCapRound: true,
                      dotData: FlDotData(
                        show: false,
                      ),
                      belowBarData: BarAreaData(
                        show: false,
                        colors: gradientColors.map((color) =>
                            color.withOpacity(0.5)).toList(),
                      ),
                    ),
                    if(itemSelectedIndex>=0)
                      LineChartBarData(
                        spots:
                        [
                          FlSpot(0, itemSelectedValue),
                          FlSpot(1, itemSelectedValue),
                          FlSpot(2, itemSelectedValue),
                          FlSpot(3, itemSelectedValue),
                          FlSpot(4, itemSelectedValue),
                          FlSpot(5, itemSelectedValue),
                          FlSpot(6, itemSelectedValue),
                          FlSpot(7, itemSelectedValue),
                          FlSpot(8, itemSelectedValue),
                          FlSpot(9, itemSelectedValue),
                          FlSpot(10, itemSelectedValue),
                          FlSpot(11, itemSelectedValue),

                        ],
                        isCurved: true,
                        barWidth: 1,
                        colors: [
                         itemSelectedColor
                        ],
                        belowBarData: BarAreaData(
                          show: false,
                          cutOffY: cutOffYValue,
                          applyCutOffY: false,
                        ),
                        aboveBarData: BarAreaData(
                          show: true,
                          colors: [Colors.red.withOpacity(0.6)],
                          cutOffY: cutOffYValue,
                          applyCutOffY: true,
                        ),
                        dotData: FlDotData(
                          show: false,
                        ),

                      ),
                  ],
                  minY: 0.0,
                    maxY: 25.0,
                  titlesData: FlTitlesData(
                    bottomTitles: SideTitles(
                      showTitles: false,
                      reservedSize: 6,
                    ),

                    leftTitles: SideTitles(
                      showTitles: false,
                    ),
                  ),
                  axisTitleData: FlAxisTitleData(
                      leftTitle: AxisTitle(
                          showTitle: false, titleText: 'Value', margin: 10),
                      bottomTitle: AxisTitle(
                          showTitle: false,
                          margin: 10,
                          titleText: 'Year',
                          textStyle: yearTextStyle,
                          textAlign: TextAlign.right)),
                  gridData: FlGridData(
                    show: false,
                  ),
                  borderData: FlBorderData(
                      show: false
                  )
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Total Spend'),
                Text('\u20B9${currentMonthResult.total.toString().
                replaceAllMapped(reg, mathFunc)}',
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold
                  ),),
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    getItemCategory('Shopping', Icons.shopping_bag_rounded,
                        Colors.red.shade900,
                        currentMonthResult.expenses.shopping,0),
                    getItemCategory('Food & Drinkes', Icons.fastfood,
                        Colors.orangeAccent.shade200,
                        currentMonthResult.expenses.food,1),
                    getItemCategory('Travel', Icons.airplanemode_active,
                        Colors.cyan.shade200,
                        currentMonthResult.expenses.travel,2),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    getItemCategory('Fuel', Icons.add_location_rounded,
                        Colors.green.shade500,
                        currentMonthResult.expenses.fuel,3),
                    getItemCategory('EMI', Icons.bookmark_rounded,
                        Colors.pink,
                        currentMonthResult.expenses.eMI,4),
                    getItemCategory('Others', Icons.shopping_cart,
                        Colors.deepPurple,
                        currentMonthResult.expenses.others,5),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          )
        ],
      );
  }

  Widget getItemCategory(String categoryItem, IconData categoryIcon,
      Color categoryIconColor, int price,int index) {
    int percentage=((price*100)/totalExpenditure).round();
    return
      InkWell(
        onTap: (){
          if(itemSelectedIndex!=index) {
            setState(() {
             itemSelectedIndex=index;
             itemSelectedColor=categoryIconColor;
             itemSelectedValue=price/1000;
            });
            scrollController.animateTo(
              0.0,
              curve: Curves.easeOut,
              duration: const Duration(milliseconds: 300),
            );
          }
          else{
            setState(() {
              itemSelectedIndex=-1;
            });
          }
        },
        child: Container(
          height: 100,
          width: 80,
          padding: EdgeInsets.only(left: 10,top:10),
          decoration: BoxDecoration(
              color: itemSelectedIndex!=index?Colors.white70:Colors.cyan.withOpacity(0.05),
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                  style: BorderStyle.none
              )
          ),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(categoryIcon,
                  color: categoryIconColor,
                  size: 30,
                ),
                SizedBox(
                  height: 10,
                ),
                Text('${categoryItem}',
                  style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.black
                  ),),
                Text('\u20B9${price.toString().
                replaceAllMapped(reg, mathFunc)}',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),),
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 5),
                      height: 5,
                      width: 24,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: categoryIconColor
                      ),
                    ),
                    Text('${percentage}%',
                      style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.black
                      ),),
                  ],
                )
              ]
          ),
        ),
      );
  }


}